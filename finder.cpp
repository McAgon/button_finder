#include "finder.h"

Area::Area()
{
    isDeleted=true;
}
Area::Area(cv::Size S)
{
    isDeleted=true;
    Points_Gray_area=cv::Mat(S.height, S.width,CV_32FC1);
    Points_area=cv::Mat(S.height, S.width,CV_32FC1);
    Start_Point=cv::Point2f(0.0,0.0);
    End_Point=cv::Point2f(0.0,0.0);
    cv::Scalar Color;


}

void Area::Del()
{
    isDeleted=true;
}

int getGlobalThreshold(cv::Mat & I)//выбор глобального порога
{

    // accept only char type matrices
    CV_Assert(I.depth() == CV_8U);
    int channels = I.channels();
    int nRows = I.rows;
    int nCols = I.cols * channels;
    if (I.isContinuous())
    {
        nCols *= nRows;
        nRows = 1;
    }
    int i,j;
    std::vector<int> hist;
    std::vector<int> accamulate_hist;
    std::vector<int> AxAccamulate_hist;
    hist.resize(256);
    accamulate_hist.resize(256);
    AxAccamulate_hist.resize(256);
    int mean1=0;
    int mean2=0;
    int n1 = 1;
    int n2 = 1;
    int threshold1;
    int best_delta_threshold=255;
    uchar* p;
    for( i = 0; i < nRows; ++i)
    {
        p = I.ptr<uchar>(i);
        for ( j = 0; j < nCols; ++j)
        {

         hist[int (*(p+j))]++;

        }

    }
    accamulate_hist[0]=hist[0];
     AxAccamulate_hist[0]=1;

    for( i = 1; i < 256; ++i)
    {


            accamulate_hist[i]=accamulate_hist[i-1]+hist[i];
            AxAccamulate_hist[i]=AxAccamulate_hist[i-1]+hist[i]*i;

    }

    for (int threshold = 1; threshold < 255; threshold++)
    {
        mean1=1;
        mean2=1;
        n1=1;
        n2=1;
        if (accamulate_hist[threshold]!=0)
        {
            mean1=AxAccamulate_hist[threshold]/accamulate_hist[threshold];
        }
        else
        {
           continue;
        }

        if (accamulate_hist[255]-accamulate_hist[threshold]!=0)
        {
            mean2=(AxAccamulate_hist[255]-AxAccamulate_hist[threshold])/(accamulate_hist[255]-accamulate_hist[threshold]);
        }
        else
        {
            continue;
        }


     //   mean1=mean1 / n1;

      if (threshold - ((mean2 + mean1) / 2)<best_delta_threshold)
      {
        best_delta_threshold = std::abs(threshold - ((mean2 + mean1) / 2));
        threshold1 = threshold;
      }

    }

 return(threshold1);
}





double FAST_calcDiffScore(cv::Mat img1,cv::Mat img2)
{
    int count=0;
    int val=0;
    int channels = img1.channels();
    int nRows = img1.rows;
    int nCols = img1.cols * channels;
    int s1=0;
    int s2=0;
    if((channels==img2.channels())&&(img1.rows==img2.rows)&&(img1.cols==img2.cols))
    {

        uchar* p1;
        uchar* p2;
        for( int i = 0; i < nRows-0; i+=2)
        {

            p1 = img1.ptr<uchar>(i);
            p2 = img2.ptr<uchar>(i);
            for (int j = 0; j < nCols; j+=2)
            {
                if (*(p1+j)!= *(p2+j))
                {
                    val++;
                }
                 if (*(p1+j)== 0)
                     s1++;
                 if (*(p2+j)== 0)
                     s2++;
                count++;
            }
            p1=NULL;
            p2=NULL;
        }


         double rez=(double) val/(count);//aposterior lier




    double ff1=(double) s1/(count);
    double ff2=(double) s2/(count);

 //   double ff=1+(std::abs(ff1-0.5))+(std::abs(ff2-0.5));//aprior lier

double rezult=rez;//*ff;
       //double rezult=0.2;

  return(rezult);
 }
    return(100);
}

double FAST_calcDiffColorScore(cv::Mat & image1, cv::Mat &image2,int step)
{

    double rez=0;
    int count=0;
       for(int y=0;y<image1.rows;y+=step)
       {
           for(int x=0;x<image1.cols;x+=step)
           {
           cv::Vec3b color1 = image1.at<cv::Vec3b>(cv::Point(x,y));
           cv::Vec3b color2 = image2.at<cv::Vec3b>(cv::Point(x,y));
           rez+=std::sqrt((color1[0]-color2[0])*(color1[0]-color2[0])+(color1[1]-color2[1])*(color1[1]-color2[1])+(color1[2]-color2[2])*(color1[2]-color2[2]));
           count++;
           }
       }

       rez/=255*count;
       return(rez);
}

void Calc_match_task(std::vector<Area>& features,  cv::Mat& Img,cv::Mat& ColorImg,int W, int H, int Step, double err_treshold,int i,std::vector<bool>& inlier, std::mutex& write_inlier_mutex,std::mutex& write_feature_mutex)
{
     cv::Point2f endMovingPoint;

     double bestDiffScore=1;
        endMovingPoint=cv::Point2f(-1,-1);
    if ((features[i].Points_Gray_area.cols>0)&&(features[i].Points_Gray_area.rows>0))
    {


     double DiffScore=0;


    cv::Mat reff_image, WBreff_image;
    cv::Mat looking_image,WBlooking_image,WBlooking_imagePart;

    reff_image=features[i].Points_Gray_area.clone();


    bool breaker=false;

        cv::Rect region ;
        region = cv::Rect(features[i].End_Point.x-floor(features[i].Part_sizeX/2), features[i].End_Point.y-floor(features[i].Part_sizeY/2), features[i].Part_sizeX,features[i].Part_sizeY);
        looking_image=Img(region).clone();
//       std::string s="/home/nik-mcagon/Pictures/";
//       s+=std::to_string(i);
//        s+="FUll.jpeg";
//        cv::imwrite(s,looking_image);
       int tresh=getGlobalThreshold(reff_image);

        cv::threshold(reff_image ,WBreff_image, tresh, 255, cv::THRESH_BINARY);
        cv::threshold(looking_image ,WBlooking_image, tresh, 255,  cv::THRESH_BINARY);
//cv::adaptiveThreshold(reff_image ,WBreff_image,255,cv::ADAPTIVE_THRESH_GAUSSIAN_C,cv::THRESH_BINARY,11,1);
//cv::adaptiveThreshold(looking_image ,WBlooking_image,255,cv::ADAPTIVE_THRESH_GAUSSIAN_C,cv::THRESH_BINARY,11,1);
//               s="/home/nik-mcagon/Pictures/";
//               s+=std::to_string(i);
//               s+=".jpeg";
//               cv::imwrite(s,WBreff_image);
    for(int dx=-floor(features[i].Part_sizeX/2);dx<floor(features[i].Part_sizeX/2)-W && !breaker;dx+=Step)
    {
        for(int dy=-floor(features[i].Part_sizeY/2);dy<floor(features[i].Part_sizeY/2)-H && !breaker;dy+=Step)
        {
            int point_x,Part_point_x;
            int point_y,Part_point_y;

            point_x=features[i].End_Point.x+dx;//coner_point
            point_y=features[i].End_Point.y+dy;//coner_point


            if(point_x<0)
                break;
            if(point_y<0)
                break;
            if(point_x+W>Img.cols)
                break;
            if(point_y+H>Img.rows)
                break;
             Part_point_x=floor(features[i].Part_sizeX/2)+dx;
             Part_point_y=floor(features[i].Part_sizeY/2)+dy;

             cv::Rect region_of_interest ;

                  region_of_interest = cv::Rect(Part_point_x, Part_point_y, W, H);

                 WBlooking_imagePart=WBlooking_image(region_of_interest);

                 DiffScore=FAST_calcDiffScore(WBreff_image, WBlooking_imagePart);



           if(DiffScore<err_treshold)
           {
               //color scoor!
               //FAST_calcDiffColorScore()

               cv::Rect colorRegion=cv::Rect(point_x,point_y,W,H);
               cv::Mat ColorSmallImg=ColorImg(colorRegion);

               DiffScore=+FAST_calcDiffColorScore(ColorSmallImg,features[i].Points_area,2)/10;

               features[i].End_Point_candidat.push_back(cv::Point2f(point_x+int(W/2),point_y+int(H/2)));
               features[i].probabiliti_of_lying_candidat.push_back(DiffScore);
               if (DiffScore<bestDiffScore)
               {

                   endMovingPoint=cv::Point2f(point_x+int(W/2),point_y+int(H/2));//center_point
                   bestDiffScore=DiffScore;
               }
           }
        }
    }

//     if(bestDiffScore<err_treshold)
//     {


//     cv::imwrite(std::string("/home/nik-mcagon/git_Odometry/odometry/Rez/Tresh/"+std::to_string(endMovingPoint.x)+std::to_string(endMovingPoint.y)+".jpeg"), best1.SrcImg);
//     cv::imwrite(std::string("/home/nik-mcagon/git_Odometry/odometry/Rez/Tresh/"+std::to_string(endMovingPoint.x)+std::to_string(endMovingPoint.y)+"(2).jpeg"), best2.SrcImg);
//     }
}
    //if(bestDiffScore<err_treshold)
    //{
        //write_feature_mutex.lock();

        features[i].End_Point=endMovingPoint;
        features[i].probabiliti_of_lying=bestDiffScore;
       // write_feature_mutex.unlock();
       // write_inlier_mutex.lock();

        inlier[i]=(bestDiffScore<err_treshold);


     //   write_inlier_mutex.unlock();
   // }


   // std::cout<<"\n DiffScore"<<endMovingPoint-features[i].Start_Point<<"with_score"<< bestDiffScore;

}

std::vector<bool> Calc_match_to_PointsArea(std::vector<Area>& features, cv::Mat& Img,cv::Mat& ColorImg,int W,int H,int Step,double err_treshold)
{
    std::mutex write_inlier_mutex;
    std::mutex write_feature_mutex;
    std::vector<std::thread> vecOfThreads;



std::vector<bool> inlier;
inlier.resize(features.size(),false);
//cv::imshow("fv0",features[10].Points_Gray_area);

    for(int i=0;i<features.size();i++)
    {

         if(!features[i].isDeleted)
         vecOfThreads.push_back( std::thread( Calc_match_task,std::ref(features),std::ref(Img),std::ref(ColorImg),W, H, Step, err_treshold,i, std::ref(inlier),std::ref(write_inlier_mutex),std::ref(write_feature_mutex)));
//Calc_match_task(features,Img,W, H, ShiftX, ShiftY, Step, err_treshold,i, inlier,write_inlier_mutex,write_feature_mutex);
    }

    for (std::thread & th : vecOfThreads)
    {

            th.join();
    }

return(inlier);
}

finder::finder()
{

}
void finder:: set_needed_Image(cv::Mat Img)
{
needed_Img=Img;


}
void catcher(cv::Mat & img1,cv::Mat & Gray_img,int RamkaX,int RamkaY,int nX,int nY,std::vector<Area>& rez,cv::Size BigSize, double scaleFACTOR=.5000, double SFstep=-0.1)
{


    int count=0;
   // double SFstep=-0.1;
    int ScaleRamkaX;
    int ScaleRamkaY;
    cv::Mat Resize_Image;
    cv::Mat Resize_Gray_img;
    int stepX;
    int stepY;
    for(double sf=1; sf>=scaleFACTOR;sf+=SFstep)
    {
        cv::resize(img1,Resize_Image,cv::Size(),sf,sf,cv::INTER_AREA);
        cv::resize(Gray_img,Resize_Gray_img,cv::Size(),sf,sf,cv::INTER_AREA);
        ScaleRamkaX=floor(RamkaX*(sf));
        ScaleRamkaY=floor(RamkaY*(sf));
        stepX=floor((BigSize.width-2*ScaleRamkaX)/nX);
        stepY=floor((BigSize.height-2*ScaleRamkaY)/nY);
        for(int i=ScaleRamkaX;i<=BigSize.width-ScaleRamkaX-stepX;i+=stepX)
        {
            for(int j=RamkaY;j<=BigSize.height-RamkaY-stepY;j+=stepY)//point in the coner
            {
                rez.resize(count+1);

                rez[count].isDeleted=false;
                rez[count].Points_area= Resize_Image;
                rez[count].Points_Gray_area= Resize_Gray_img;
                rez[count].Start_Point=cv::Point2f(i+floor(stepX/2),j+floor(stepY/2));//point in the center
                rez[count].End_Point=rez[count].Start_Point;
                  rez[count].Part_sizeX=stepX+2*ScaleRamkaX;
                  rez[count].Part_sizeY=stepY+2*ScaleRamkaY;
                count++;
            }
        }
    }

   // rez.resize(count);

}
std::vector<cv::Point2f> finder::find_on_img(cv::Mat LookingImg)
{

std::vector<Area> features;

cv::Mat Gray_needed_Img, Gray_LookingImg;
 Gray_needed_Img=cv::Mat(needed_Img.cols,needed_Img.rows,needed_Img.type());
 cv::cvtColor(needed_Img, Gray_needed_Img,CV_RGB2GRAY);
 cv::cvtColor(LookingImg, Gray_LookingImg,CV_RGB2GRAY);


catcher(needed_Img, Gray_needed_Img,needed_Img.cols,needed_Img.rows,2,2,features,cv::Size(LookingImg.cols,LookingImg.rows));

Calc_match_to_PointsArea(features,Gray_LookingImg,LookingImg,needed_Img.cols,needed_Img.rows,1,0.1);
double best_prob=1;
double best_i=0;
for(int i=0;i<features.size();i++)
{

        if (features[i].probabiliti_of_lying<best_prob)
        {
            best_prob=features[i].probabiliti_of_lying;
       best_i=i;
        }
}
std::vector<cv::Point2f> rez;
for(int i=0;i<features.size();i++)
{
    for(int j=0;j<features[i].End_Point_candidat.size();j++)
    {
        if(features[i].probabiliti_of_lying_candidat[j]<=best_prob*1.000001)
        {
            rez.push_back(features[i].End_Point_candidat[j]);
        }
    }
}
  rez.push_back(features[best_i].End_Point);
//return(features[best_i]);
return(rez);
}




