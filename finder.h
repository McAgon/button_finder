#ifndef FINDER_H
#define FINDER_H
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/features2d.hpp"
//#include "opencv2/xobjdetect.hpp"
#include <thread>
#include <mutex>
#include <opencv2/imgproc.hpp>
#include "opencv2/core/mat.hpp"
#include <opencv/cv.h>
#include <opencv2/features2d/features2d.hpp>
#include "opencv2/calib3d.hpp"


#include <iostream>
#include <sys/types.h>
#include <dirent.h>
#include <fstream>

struct box
{

public:
      box()
      {
          Central_point=cv::Point(0,0);
          W=0;
          H=0;
      }
    cv::Point2i Central_point;
    int W;
    int H;

};

class Area
{

    public:
    int tresh;


     cv::Mat WBPoints_area;
     double probabiliti_of_lying;
     cv::Mat Points_area;
     cv::Mat Points_Gray_area;
     cv::Point2f Start_Point;
     std::vector<cv::Point2f> End_Point_candidat;
     std::vector<double> probabiliti_of_lying_candidat;

     cv::Point2f End_Point;
     bool isDeleted;
     int Part_sizeX;
     int Part_sizeY;
     Area();
     Area(cv::Size S);
     void Del();
     cv::Vec3b Color;
};


class finder
{
public:
    finder();
 void   set_needed_Image(cv::Mat Img);
 std::vector<cv::Point2f> find_on_img(cv::Mat Img);
 box   get_rezbox();
private :
 cv::Mat needed_Img;
 cv::Mat BW_needed_Img;

};

#endif // FINDER_H
